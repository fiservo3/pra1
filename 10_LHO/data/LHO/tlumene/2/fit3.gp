set fit logfile '3.log'
set samples 1000

omega = 2.5
q=-0.13
a = 1
alfa = 1
lambda = 1

f(x) = a*exp(- lambda * x) * cos (omega*x + alfa) + q

fit f(x) '3.txt' using 1:2 via a, lambda, omega, alfa, q

plot '3.txt' using 1:2 title "změřené body", f(x) title "fit"