set fit logfile '5.log'
set samples 1000

omega = 1
q=-0.13
a = 1
alfa = 1
lambda = 1

f(x) = a*exp(- lambda * x) * cos (omega*x + alfa) + q

fit f(x) '5.txt' using 1:2 via a, lambda, omega, alfa, q

plot '5.txt' using 1:2 title "data, ktery jsem si vycucal z prstu", f(x) title "fit"