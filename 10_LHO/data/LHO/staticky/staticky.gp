set encoding utf8
k = 0.01
q = 1

f(x) = -9.81373*k**-1*x  + q
fit f(x) 'staticky.txt' using 1:2:3 yerrors via k, q

set xlabel "m [kg]"
set ylabel "l [m]"

set xrange [-0.004:0.072]
plot 'staticky.txt' using 1:2:3 with yerrorbars title "Změřené body", f(x) title "Lineární fit"
