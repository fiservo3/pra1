set terminal pdfcairo 
set output 'orient2.pdf'
set size ratio 0.2
set xtics 0, 10, 350
set tics scale 1
set grid x
set xtics font ", 8"
plot '2.txt' using 1:2 notitle
