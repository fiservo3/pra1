import numpy as np 
import math

F_v = 100 	#frekvence vzorkovani [Hz], nemenit
usek = 2.5	#kolik trva jeden usek [s], menit jen opatrne

def max (zacatek_cas, konec_cas):
	global F_v
	global data
	zacatek = zacatek_cas * F_v
	konec = konec_cas * F_v

	max = data[0, 1]
	i = int(zacatek)
	while(i<konec):
		if(data[i, 1] > max):
			max = data[i, 1];
		i+=1
	return max

def min (zacatek_cas, konec_cas):
	global F_v
	global data
	zacatek = zacatek_cas * F_v
	konec = konec_cas * F_v

	min = data[0, 1]
	i = int(zacatek)
	while(i<konec):
		if(data[i, 1] < min):
			min = data[i, 1];
		i+=1
	return min

def rozkmity (zacatek_cas, konec_cas):
	global usek;
	n = int((konec_cas - zacatek_cas)/usek)
	hodnoty = np.zeros(n)

	i=0
	while (i<n):
		hodnoty[i] = max(zacatek_cas+i*usek, zacatek_cas+(i+1)*usek) - min(zacatek_cas+i*usek, zacatek_cas+(i+1)*usek)
		i+=1

	return hodnoty

def prumer (data):
	suma = 0
	n=data.shape[0]
	for cislo in data:
		suma += cislo
	prumer = suma/n

	sumaOdchylek2 = 0
	for cislo in data:
		sumaOdchylek2 += (cislo-prumer)**2
	odchylka = math.sqrt(sumaOdchylek2/((n-1)*n))
	return (prumer, odchylka)


rozdeleni = np.loadtxt("rozdeleni_2.txt")
n=rozdeleni.shape[0]

data = np.loadtxt("2.txt")
#print(rozdeleni)


i=0
while(i<n):
	meze = rozdeleni[i, :]
	delka = meze [1] - meze[0]
	print (prumer(rozkmity(meze[0], meze[1])))
	i+=1