set encoding utf8

set xlabel "f [Hz]"
set ylabel "b [mm]"

set samples 100000

m=0.5

omega = 2.52
lambda = 0.1

f(x) = 1/(m*sqrt((omega**2 - x**2)**2+4*lambda**2*x**2))

fit f(x) '2_prepsane.txt' using 1:2:3 yerrors via m, omega, lambda

plot '2_prepsane.txt' using 1:2:3 with yerrorbars title "spočtené amplitudy", f(x) title " fit b(γa)"