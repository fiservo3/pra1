import math

#tady zadam namerene hodnoty
mp = 0.3344
m1 = 0.37994
mk = 0.0707

t1 = 24.49
t2 = 100
t= 30.94

sigma_t1 = 0.02
sigma_t = 0.03


mv = m1-mk
sigma_m	= 1e-5

sigma_Ck = 6

cv = 4180
Ck = 83

sigma_delat_t = math.sqrt(sigma_t**2 + sigma_t1**2)
sigma_delat_m = math.sqrt(2)*sigma_m


Ccelk = mv*cv + Ck 
sigma_Ccelk = math.sqrt(sigma_Ck**2 + sigma_m**2*cv**2)

cp=((t-t1)/(t2-t))*((Ccelk)/mp)


prisp_dt = (1 + (t-t1)/(t2-t))*(Ccelk/(mp*(t2-t)))
prisp_dc = (t-t1)/((t2-t)*Ccelk)
prisp_mp = ((t-t1)/(t2-t))*(Ccelk/mp**2)

sigma_cp = math.sqrt((sigma_delat_t*prisp_dt)**2 + (sigma_Ccelk*prisp_dc)**2 + (sigma_m*prisp_mp)**2)

print ('c = ' + str(cp) +  '+-' + str(sigma_cp))