import math
import numpy as np

#koeficienty primek, ty mi vyplivnul gnuplot

k2 = -0.00099393
q2 = 31.148
k1 = -0.000806823
q1 = 24.6506

sigma_k2 = 2.404e-05
sigma_q2 = 0.01212 
sigma_k1 = 0.0001022 
sigma_q1 = 0.009373


def lichobezink (primka, bod1, bod2):
	if (primka == 1):
		primka1 = bod1[0]*k1 + q1
		primka2 = bod2[0]*k1 + q1

	if (primka == 2):
		primka1 = bod1[0]*k2 + q2
		primka2 = bod2[0]*k2 + q2
	
	deltaX  =bod2[0] - bod1[0]
	deltay1 =bod1[1]- primka1
	deltay2 =bod2[1]- primka2

	S = deltaX * ((deltay1 + deltay2)/2)
	return (S)

def integral (primka, dolniMez, horniMez, data):
	if (horniMez == dolniMez): 
		return (0)
	bod1 = (dolniMez, data[dolniMez, 0])
	bod2 = (dolniMez+1, data[dolniMez+1, 0])
	return (lichobezink (primka, bod1, bod2) + integral (primka, dolniMez+1, horniMez, data))
	

data = np.loadtxt('data.txt')
n = data.shape[0]
novadata = np.zeros((n, 4), dtype = float)
novadata[:,:-1] = data
#print (data)

zacatek = 32
konec = 50

minimum =10000000000
pozice = 0

i = zacatek
while (i < konec):
	a = integral (1, zacatek, i, data)
	b = integral (2, i, konec, data)
	novadata [i, 3] = abs(a-b)
	if (abs(a-b) < minimum):
		minimum = abs(a-b)
		pozice = i

	i +=1

cas = data[pozice, 0]

t1 = k1*cas + q1
t= k2*cas + q2

sigma_t1 = math.sqrt(cas**2*(sigma_k1)**2 + sigma_q1**2)
sigma_t = math.sqrt(cas**2*(5*sigma_k2)**2 + sigma_q2**2)

print('t1= ' + str(t1) + ' +- ' + str(sigma_t1))
print('t= ' + str(t) + ' +- ' + str(sigma_t))

np.savetxt("novaData.txt", novadata, fmt="%6g")