import numpy as np

def koef (bod1, bod2):
	x1 = bod1[0]
	x2 = bod2[0]
	y1 = bod1[1]
	y2 = bod2[1]
	 
	q=(x2*y1-x1*y2)/(x2-x1)
	k = (y1-q)/x1

	return ((k, q))

#tady zadavam body pro kalibraci
led = (0.3, 0)
var = (100.6, 100)

koeficienty = koef(led, var)

print ('kalibrační přímka: ')
print ('y = ' + str(koeficienty[0]) + ' x + ' + str(koeficienty[1]))

stara = np.loadtxt('data.txt')
n=stara.shape[0]
i=0

nova = np.zeros((n, 2), dtype=float)
nova [:, 0] = stara [:, 0] #casy netreba prepocitat, tak si je proste nakopiruju

while (i<n):
	nova [i, 1] = koeficienty[0]*stara[i, 1] + koeficienty[1]
	i+=1


np.savetxt("okalibrovana.txt", nova, fmt="%6g")