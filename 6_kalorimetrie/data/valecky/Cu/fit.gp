set encoding utf8
#set terminal pdfcairo
#set output '5.pdf' #výstupní soubor


set xlabel 't [s]'
set ylabel 'T [°C]' #neni otoceno

set key right bottom

tit_teplota = "změřená teplota"
tit_fit1 = "fit teploty před"
tit_fit2 = "fit teploty po"


konec = 230
zacatek = 320


f(x) = k1*x +q1
g(x) = k2*x +q2
fit [0:konec] f(x) './data.txt' using 1:2 via k1,q1
fit [zacatek:] g(x) './data.txt' using 1:2 via k2,q2

set grid x

set style line 3 lt 2 lw 1 pt 3 ps 0.5 dt 2 lc 3
set style line 4 lt 2 lw 1 pt 3 ps 0.5 dt 4 lc 2

set style line 5 lt 2 lw 2.5 pt 3 ps 0.5 dt 3 lc 3
set style line 6 lt 2 lw 2.5 pt 3 ps 0.5 dt 2 lc 2

plot './data.txt' using 1:2:3 with yerrorbars title tit_teplota, \
 [0:konec] f(x) notitle ls 5, [konec:zacatek] f(x) title tit_fit1 ls 3,\
[zacatek:] g(x) notitle ls 6, [konec:zacatek] g(x) title tit_fit2 ls 4
#exit