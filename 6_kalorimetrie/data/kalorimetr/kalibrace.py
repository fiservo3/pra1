import numpy as numpy

def koef (bod1, bod2):
	k=(bod1[0]-bod2[0])/(bod1[1]-bod2[1])
	q=bod1[1] - bod1[0]*k
	return ((k, q))

#tady zadavam body pro kalibraci
led = (0, 0.3)
var = (100, 100.6)

koeficienty = koef(led, var)

print ('kalibrační přímka: ')
print ('y = ' + str(koeficienty[0]) + ' x + ' + str(koeficienty[1]))

stara = np.loadtxt('data.txt')
n=stara.shape[0]
i=0

nova = zeros(n, 2)
nova [:, 0] = stara [:, 0] #casy netreba prepocitat, tak si je proste nakopiruju

while (i<n):
	nova [i, 1] = koeficienty(0)*stara[i, 1] + koeficienty(1)
	i+=1


np.savetxt("okalibrovana.txt", nova, fmt="%6g")