import math

def koef (bod1, bod2):
	x1 = bod1[0]
	x2 = bod2[0]
	y1 = bod1[1]
	y2 = bod2[1]
	 
	q=(x2*y1-x1*y2)/(x2-x1)
	k = (y1-q)/x1

	return ((k, q))

def okalibruj (hodnota, koeficienty):
	return (koeficienty[0]*hodnota + koeficienty[1])

#tady zadavam body pro kalibraci
led = (0.3, 0)
var = (100.6, 100)

#tady zadam namerene hodnoty
m = 0.0707
m1 = 0.2118
m2 = 0.36288


t1 = 21.9
t2 = 100
t= 60.0



sigma_m	= 1e-5
sigma_t = 0.1

cv = 4180

#tady to prekalibruje hodnoty teploty
koeficienty = koef(led, var)
t = okalibruj(t, koeficienty)
t1 = okalibruj(t1, koeficienty)
#m2 = okalibruj(m2, koeficienty)

sigma_delat_t = math.sqrt(2)*sigma_t
sigma_delat_m = math.sqrt(2)*sigma_m

C = cv*(((m2 -m1)*(t2-t))/(t-t1) - (m1-m))

sigma_c = cv*math.sqrt(sigma_delat_m**2 + sigma_delat_m**2 *((t2-t)/(t-t1))**2 + sigma_delat_t**2 * (((m-m1)/(t-t1)) + (((m-m1)*(t2-t)/(t-t1)**2)) )**2)
print(t1)
print(t2)
print(t)

print (4200 * math.sqrt (sigma_delat_t**2 * (((m-m1)/(t-t1)) + (((m-m1)*(t2-t)/(t-t1)**2)) )**2))

print ('C = ' + str(C) +  '+-' + str(sigma_c))