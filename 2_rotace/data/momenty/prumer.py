import numpy as np
import math

#sem zadam konstanty
#g jako cislo, m jako tuple s hodnotou a chybou
#g [ms^-2], m [kg], r[mm]
g = 9.81				#grav. zrychleni
m = (0.19946, 2e-5)		#hmotnost zavazi
d = (3.04, 0.1)			#prumer civky



r = (d[0]/200, d[1]/200)

def prumer (pole):
	radky = pole.shape[0]
	#print(radky)
	sumpx = 0
	sump = 0
	i=0
	while i<radky :
		p=1/(pole[i, 1]*pole[i, 1])
		sump = sump + p
		sumpx = sumpx + p*pole[i, 0]
		i=i+1
	#print ('prumer: ')
	#print (sumpx/sump)
	#print ('chyba: ')
	#print (math.sqrt(1/sump))
	return (sumpx/sump, math.sqrt(1/sump))

def moment (eps):
	I = r[0]*m[0]*((g)/(eps[0]) - r[0])
	#Si = math.sqrt(m[0]**2*((g)/(eps[0])-2r[0] * r[1])**2 + (r[0]*m*g[0]*eps[0]**(-2)*eps[1])**2 + (r[0]*((g/eps[0]) - r[0])*m[1])**2)
	didr=m[0]*((g/eps[0]) - 2*r[0])
	didm=r[0]*((g/eps[0])-r[0])
	dide=r[0]*m[0]*g*(eps[0]**(-2))
	Si = math.sqrt((didr * r[1])**2 + (dide*eps[1])**2 + (didm*m[1])**2)
	return (I, Si)

data = np.loadtxt('u_kraje.txt')
eps = prumer(data)
vysledek = moment(eps)
#print (r)
#print ('eps (' + str(vysledek[0]) + '+-' + str(vysledek[1]) + ') jednotek')
print (eps)
print (vysledek)