set encoding utf8
set terminal pdfcairo
set termoptions enhanced
set output 'lehci.pdf' #výstupní soubor

set xlabel 'statovací rychlsot [ms^{-1}]'
set ylabel 'odchylka rychlsoti [ms^{-1}]'
set y2label 'relativní odchylka rychlsoti [1]'
set y2tics
set y2range [0:0.022]
set yrange [0:0.005]

plot './lehci.txt' using 1:2 title 'absolutní', './lehci.txt' using 1:3 title 'relativní' axes x1y2

exit