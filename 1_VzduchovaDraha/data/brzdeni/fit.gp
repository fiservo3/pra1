set encoding utf8
set terminal pdfcairo
set output '1.pdf' #výstupní soubor
set ytics rotate by 90

set y2range [1.8:2]
#set y2tics rotate by 90
set y2tics 1.8,0.05,20

set xlabel 't [s]'
set ylabel 'vzdálenost [m]'


tc_v = 50



start = 4
konec = 6

m=.30526
k=0.00001
b=1
a=.3
f(x) = (x*tc_v)**2*((k)/(2*m)) + a*x + b
#f(x) = a*x + b
fit [start:konec] f(x) './vzdal.txt' using 1:2 via a, b,k

set grid x
set xrange [start:konec]

plot './vzdal.txt' using 1:2 every ::start*tc_v::konec*tc_v title "změřená vzdálenost",\
[start:konec] f(x) title "fit"
#exit