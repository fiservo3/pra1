set encoding utf8
set terminal pdfcairo
set output 'celk.pdf' #výstupní soubor

set xtics 0,5,200

set xlabel 't [s]'
set ylabel 'vzdálenost [m]'

tit_sila = "síla"
tit_vzdal = "vzdálenost"

set grid x
plot './vzdal.txt' using 1:2 title tit_vzdal w l
exit