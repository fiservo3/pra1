set encoding utf8
set terminal pdfcairo
set output 'impuls.pdf' #výstupní soubor

set key left

set xlabel "T [J]"
set ylabel "T’ [J]"
plot 'hybnost.txt' with xyerrorbars,\
  x w l title "ideální stav"
exit
