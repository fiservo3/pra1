set encoding utf8
set terminal pdfcairo
set output '3.pdf' #výstupní soubor
set ytics rotate by 90

set y2range [1.8:2]
#set y2tics rotate by 90
set y2tics 1.8,0.05,20

set xlabel 't [s]'
set ylabel 'síla [N]' #neni otoceno
set y2label 'vzdálenost [m]'

set fit logfile '3.log'

tit_sila = "síla"
tit_vzdal = "vzdálenost"
tit_fit1 = "fit rychlosti před"
tit_fit2 = "fit rychlosti po"

tc_s = 1000
tc_v = 50


bum = 25.45


start = bum-1.1
konec = bum+1.1


v1 = 1
v2 = -1
k1=1
k2=1

f(x) = v1*x +k1
g(x) = v2*x +k2
fit [start:bum-.1] f(x) './vzdal.txt' using 1:2 via v1,k1
fit [bum+.3:konec] g(x) './vzdal.txt' using 1:2 via v2,k2

set grid x
set xrange [start:konec]

plot './sila.txt' using 1:2 every ::start*tc_s::konec*tc_s title tit_sila with lines, \
'./vzdal.txt' using 1:2 every ::start*tc_v::konec*tc_v title tit_vzdal axes x1y2,\
[start:bum-.1] f(x) title tit_fit1 axes x1y2 , [bum+.3:konec] g(x) title tit_fit2 axes x1y2

#exit