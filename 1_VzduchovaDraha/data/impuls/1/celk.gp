set encoding utf8
set terminal pdfcairo
set output 'celk.pdf' #výstupní soubor
set ytics rotate by 90

set y2range [1.8:2]
#set y2tics rotate by 90
set y2tics 0.2,0.5,20
set xtics 0,10,200

set xlabel 't [s]'
set ylabel 'síla [N]' #neni otoceno
set y2label 'vzdálenost [m]'

tit_sila = "síla"
tit_vzdal = "vzdálenost"

set grid x
plot './vzdal1.txt' using 1:2 title tit_vzdal 
exit