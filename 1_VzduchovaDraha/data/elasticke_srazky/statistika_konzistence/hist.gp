#nastavení základních věcí pro gnuplot

set encoding utf8
set terminal pdfcairo
set termoption enhanced
set output 'histogram.pdf' #výstupní soubor

#nastavení věcí pro počítání binů
n=6 #počet 
max=.3 #max value
min=.24 #min value
width=(max-min)/n #interval width
#function used to map a value to the intervals
hist(x,width)=width*floor(x/width)+width/2.0



#nastavit, jak budou vypadat osy...

set xrange [0.23:0.26]
set yrange [0:5]

set offset graph 0.05,0.05,0.05,0.0#rámeček kolem grafu

set ytics 0,1,5
set xtics min,(max-min)/6,max-0.001
set boxwidth width*0.9
set style fill solid 0.5 #fillstyle
set tics out nomirror
set xlabel "rychlost[ms^{-1}]"
set ylabel "četnost"


#spočítat a vykreslit
plot "dta_hist1.txt" u (hist($1,width)):(1.0) smooth freq w boxes lc rgb"blue" notitle