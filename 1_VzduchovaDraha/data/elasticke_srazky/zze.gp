set encoding utf8
set terminal pdfcairo
set output 'zze.pdf' #výstupní soubor

set key left

set xlabel "T [J]"
set ylabel "T’ [J]"
plot 'data_graf_t.txt' using 1:3:2:4 with xyerrorbars title "konfigurace 1",\
 'data_graf_t2.txt' using 1:3:2:4 with xyerrorbars title "konfigurace 2",\
  x w l title "ideální stav"
exit
