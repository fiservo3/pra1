set encoding utf8
set terminal pdfcairo
set output 'zzh_1.pdf' #výstupní soubor

set key left

set xlabel "p [kgm/s]"
set ylabel "p’ [kgm/s]"
plot 'data_graf_p.txt' using 1:3:2:4 with xyerrorbars title "konfigurace 1",\
 'data_graf_p2.txt' using 1:3:2:4 with xyerrorbars title "konfigurace 2",\
  x w l title "ideální stav"
exit
