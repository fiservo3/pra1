set encoding utf8
set terminal pdfcairo
set output 'zzh_2.pdf' #výstupní soubor

set xlabel "p [gm/s]"
set ylabel "p’ [gm/s]"
plot 'data_graf_p2.txt' using 1:3:2:4 with xyerrorbars, x w l
exit
