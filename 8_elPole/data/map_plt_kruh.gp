
set dgrid3d 12,12
set contour
set pm3d

set xlabel "x"
set ylabel "y"
set zlabel "φ [v]"


splot 'kruh_prepsana.txt' title "potenciál el. pole kruhových elektrod"