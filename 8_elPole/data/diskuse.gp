set encoding utf8

set ylabel "E[V/m]"
set xlabel "d[mm]"

plot 'diskuse.txt' using 1:5:6 with yerrorbars notitle