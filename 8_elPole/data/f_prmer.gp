set encoding utf8

set xrange [0:1.1]

set xlabel "s/D"
set ylabel "f(s/D)"

a=0.5
f(x) = exp(a*x)
fit f(x) 'funkce_prumery.txt' using 1:2:3 yerrors via a 



plot 'funkce_prumery.txt' using 1:2:3 with yerrorbars dt 2 title "spočtená hodnota f(s/D)",\
f(x) title "fit exp(a s/D)"
exit