set encoding utf8

set xrange [0:1.1]

a=0.5
f(x) = exp(a*x)
fit f(x) 'funkce_data.txt' using 1:5:8 yerrors via a 


set xlabel "s/D"
set ylabel "f(s/D)"


plot 'funkce_data.txt' using 1:5:8 with yerrorbars dt 2 title  "spočtená hodnota f(s/D), první měření",\
'funkce_data.txt' using 1:6:9 with yerrorbars dt 3 title "spočtená hodnota f(s/D), druhé měření",\
'funkce_data.txt' using 1:7:10 with yerrorbars dt 4 title "spočtená hodnota f(s/D),třetí měření",\
#f(x)
exit