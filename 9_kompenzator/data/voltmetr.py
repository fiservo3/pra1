import numpy as np 
import matplotlib
from matplotlib import pyplot as plt

Rn = 1000


data = np.loadtxt('voltmetr.txt')
n=data.shape[0]	#pocet hodnot

kalibrace = np.zeros((n, 2), dtype = float)

i=0
while (i<n):
	kalibrace [i, :] = [data[i, 2], data[i, 1]*data[i, 0]]
	i+=1


print (data)
print (kalibrace)

fig=plt.figure()
axes = fig.add_axes(0.1,0.9,0.1,0.9)
axes.plot(kalibrace[:, 1], kalibrace[:, 0])

fig.show()

exit