set encoding utf8

set ylabel "ΔR [Ω]"
set xlabel "nastavená hodnota na dekádě [Ω]"

set xrange [50:1050]
#set yrange [50:1050]

set grid

set key top left
plot 'korekce_R.txt' using 1:10 notitle  ps 2
