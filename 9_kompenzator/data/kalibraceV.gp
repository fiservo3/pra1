set encoding utf8

set xlabel "U [V]"
set ylabel "zobrazená hodnota voltmetru [V]"

q=0.0001
k=1
f(x)= k*x+q

fit f(x) 'kalibrace_V.txt' using 2:1 via k, q

set xrange [-0.1:10.1]
set yrange [-0.1:10.3]

set grid

plot 'kalibrace_V.txt' using 2:1  notitle w linespoints linetype 1 ps 2,'' using 2:1:3 with yerrorbars linetype 1 notitle, f(x) title "fit k nicemu"
