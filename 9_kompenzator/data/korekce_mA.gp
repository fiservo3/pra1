set encoding utf8

set ylabel "ΔI [mA]"
set xlabel "zobrazená hodnota na miliampérmetru [mA]"

set xrange [0.5:1.03]
#set yrange [0.5:1.03]

set grid

plot 'korekce_mA.txt' using 2:3  notitle w linespoints linetype 1 ps 2,