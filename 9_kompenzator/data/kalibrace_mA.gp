set encoding utf8

set xlabel "I [mA]"
set ylabel "zobrazená hodnota na miliampérmetru [mA]"

set xrange [0.5:1.03]
set yrange [0.5:1.03]

set grid

plot 'kalibrace_mA_sort.txt' using 1:2  notitle w linespoints linetype 1 ps 2,'' using 1:2:3 with yerrorbars linetype 1 notitle
