set encoding utf8

set xlabel "R [Ω]"
set ylabel "nastavená hodnota na dekádě [Ω]"

set xrange [50:1050]
set yrange [50:1050]

set grid

set key top left
plot 'dekada.txt' using 9:1 title "naměřené hodnoty" w linespoints linetype 1 ps 2 dt 2, '' using 10:1 title "s vynecháním podezřelých hodnot" w linespoints
