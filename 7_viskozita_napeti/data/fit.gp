set encoding utf8
set terminal pdfcairo
set output "graf.pdf"

set xlabel "V_t [m^3 s^{-1}]"
set ylabel "(p_1^2-p_2^2)/(2p_2) [Pa]"
titulek = "změřené hodnoty"

pi = 3.1415926535
eta = 1.5e-5
r = 0.39e-3
a = 91.6e-3

f(x) = x*((8*a*eta)/(pi*r**4))
k=1
#f(x) = k*x
fit f(x) 'plyn.txt' using 1:4 via eta

set key top left

plot 'plyn.txt' using 1:4:5 with yerrorbars title titulek, f(x)
exit