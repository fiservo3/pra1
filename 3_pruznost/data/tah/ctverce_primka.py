import numpy as np
import math

#pomocna funkce, ktera mi secte prvky vektoru
def suma (vektor):
	soucet = 0
	for prvek in vektor:
		soucet += prvek
	return soucet


def primka (soubor):
	sigma_y = 0.005

	n=soubor.shape[0]
	#udelam si nove pole o stejne vysce a dvou sloupcich
	z=np.zeros((n, 5), dtype=float)

	#ted si do nej pro kazde i naladuju hodnoty x_i*y_i a x_i^2
	#a pro vypocet chyby i x^.(s_x)^2 a 
	i=0
	while(i<n):
		radek = soubor[i,:]
		z[i,:] = np.array((radek[0]**2, radek[0]*radek[1], radek[0]**2*radek[2]**2, radek[1]**2*radek[2]**2+sigma_y*radek[0]**2, radek[0]), dtype=float)
		#print (np.array((radek[0]**2, radek[0]*radek[1], radek[0]**2*radek[2]**2, radek[1]**2*radek[2]**2+sigma_y*radek[0]**2)))
		
		i +=1

	k=(n*suma(z[:,1])-suma(soubor[:,0]) * suma(soubor[:,1]))/(n*suma(z[:,0]) - suma(soubor[:,0])**2)
	q=(suma(z[:,0])*suma(soubor[:,1]) - suma(soubor[:,0])*suma(z[:,1]))/(n*suma(z[:,0]) - suma(soubor[:,0])**2)
	
	i=0
	while(i<n):
		z[i,:] = np.array((radek[0]**2, radek[0]*radek[1], radek[0]**2*radek[2]**2, radek[1]**2*radek[2]**2+sigma_y*radek[0]**2, (radek[1] - k*radek[0] - q)**2))
		i +=1

	sigma_suma_x = 0.0316
	sigma_suma_y = 0.2336
	sigma_suma_x2 = math.sqrt(2* suma(z[:,2]))
	sigma_suma_xy = math.sqrt(suma(z[:,3]))


	citatel_ctverec = (n*suma(z[:,1])-suma(soubor[:,0]) * suma(soubor[:,1]))**2
	jmenovatel_ctverec  = (n*suma(z[:,0]) - suma(soubor[:,0])**2)**2
	sigma_cit_ctverec = n**2 * sigma_suma_xy**2 + suma(soubor[:,1])*sigma_suma_x**2 + suma(soubor[:,1])*suma(soubor[:,0])**2 * sigma_suma_y**2
	sigma_jmen_ctverec = n**2 * sigma_suma_x2**2 + (2* suma(soubor[:,0]) * sigma_suma_x)**2

	#sigma_k = math.sqrt(sigma_cit_ctverec * jmenovatel_ctverec + sigma_jmen_ctverec * citatel_ctverec)
	male_s = math.sqrt((math.sqrt(suma(z[:,4])))/(n-2))
	sigma_k = male_s * math.sqrt((n)/(n*suma(z[:,0])-(suma(soubor[:,0]))**2))
	return (k, q, sigma_k)

data = np.loadtxt('pridavani.txt')
#print(data)
vysledek = primka(data)
print (str(vysledek[0]) + 'x + ' + str(vysledek[1]))
print ('chyba smernice k: ' + str(vysledek[2]) + ', chybu q toto nepocita')