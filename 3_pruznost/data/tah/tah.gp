set encoding utf8
set terminal pdfcairo
set output 'tah.pdf' #výstupní soubor
#set ytics rotate by 90

set xlabel "zatížení [g]"
set ylabel "protažení [mm]"

f(x) = 0.00136014818284*x - 0.00544091883194
g(x) = 0.00137397596901*x + 0.00129210722803


set grid x
set grid y

set key top left

plot './tah.txt' using 2:5:3:6 title "protažení drátu" with xyerrorbars,\
f(x) title "regresní přímka pro přidávání závaží" dashtype 4,\
g(x) title "regresní přímka pro odebírání závaží"dashtype 6

#exit