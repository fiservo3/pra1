set encoding utf8
set terminal pdfcairo
set output 'graf.pdf' #výstupní soubor
#set ytics rotate by 90

set xlabel "zatížení [kg]"
set ylabel "otočení [rad]"

f(x) = 1.09123429333*x +4.67140020639



set grid x
set grid y

set key top left

plot './data.txt' using 1:2 title "otočení",\
f(x) title "regresní přímka pro přidávání závaží" dashtype 4,\


#exit