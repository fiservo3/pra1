import numpy as np
import math

#pomocna funkce, ktera mi spoctou sumy, ktere potrbuji
def suma_x (soubor):
	n=soubor.shape[0]
	soucet = 0
	i = 0
	while(i<n):
		soucet += soubor[i,0]
		i+=1
	return soucet

def suma_y (soubor):
	n=soubor.shape[0]
	soucet = 0
	i = 0
	while(i<n):
		soucet += soubor[i,1]
		i+=1
	return soucet

def suma_x2 (soubor):
	n=soubor.shape[0]
	soucet = 0
	i = 0
	while(i<n):
		soucet += (soubor[i,0])**2
		i+=1
	return soucet

def suma_xy (soubor):
	n=soubor.shape[0]
	soucet = 0
	i = 0
	while(i<n):
		soucet += (soubor[i,1]*soubor[i,0])
		i += 1
	return soucet

def chybovy_soucet_ctvercu (soubor, k, q):
	n=soubor.shape[0]
	soucet = 0
	i=0
	while(i<n):
		soucet += (soubor[i,1] - k*soubor[i,0] - q)
		i += 1
	return soucet






def primka (soubor):
	n=soubor.shape[0]

	k=(n*suma_xy(soubor) - suma_y(soubor)*suma_x(soubor))/(n*suma_x2(soubor) - suma_x(soubor)**2)
	q=(suma_x2(soubor)*suma_y(soubor) - suma_x(soubor)*suma_xy(soubor))/(n*suma_x2(soubor) - suma_x(soubor)**2)

	S = chybovy_soucet_ctvercu(soubor, k, q)
	s = math.sqrt(S/(n-2))

	sigma_k = s*math.sqrt(n/(n*suma_x2(soubor)-suma_x(soubor)**2))
	sigma_q = s*math.sqrt(suma_x2(soubor)/(n*suma_x2(soubor)-suma_x(soubor)**2))

	return (k, sigma_k, q, sigma_q)


data = np.loadtxt('data.txt')
#print(data)
vysledek = primka(data)
print ('k= ' + str(vysledek[0]) + ' \pm ' + str(vysledek[1]))
print ('q= ' + str(vysledek[2]) + ' \pm ' + str(vysledek[3]))