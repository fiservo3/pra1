set encoding utf8

Imaxb=36
Qb=2.8
f0b = 195

Ib(x) = Imaxb/(sqrt(1+Qb**2*(x/f0b - f0b/x)**2))

Imaxs=40
Qs=1.9
f0s = 200

Is(x) = Imaxs/(sqrt(1+Qs**2*(x/f0s - f0s/x)**2))

fit Ib(x) 'bez.txt' using 1:2 via Imaxb, Qb, f0b
fit Is(x) 's.txt' using 1:2 via Imaxs, Qs, f0s
set logscale y
set yrange [16:40]

set ytics(15,20,25,30,35,40)

set xlabel "f [kHz]"
set ylabel "I_a [mA]"


plot 'bez.txt' using 1:2 title "změřené hodnoty pro cívku bez jádra", 's.txt' using 1:2 title "změřené hodnoty pro cívku s jádrem",\
 Ib(x) title "fit I(ω) pro cívku bez jádra", Is(x) title "fit I(ω) pro cívku s jádrem"