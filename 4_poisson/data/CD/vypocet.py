import numpy as np
import math

#konstanty
g = 9.81373
ro = 998.205
sigma_h = 0.3
sigma_p = g*ro*sigma_h*0.01

def poisson (h1, h2):
	p1 = g*ro*h1*0.01
	p2 = g*ro*h2*0.01

	kappa = p1/(p1-p2)
	prisp_p1 = (p2)/(p1-p2)**2
	prisp_p2 = (p1)/(p1-p2)**2
	sigma_kappa = math.sqrt(prisp_p1**2 + prisp_p2**2) * sigma_p
	return (kappa, sigma_kappa)



data = np.loadtxt('zmerena.txt')
n=data.shape[0]
#print(data)

spoctene = np.zeros((n, 5), dtype=float)

i=0
while (i<n):
	#debug
	#print (poisson(data[i,0], data[i,1]))
	vysledek = poisson(data[i,0], data[i,1])
	spoctene[i,:] = (data[i,0], data[i,1], data[i,2], vysledek[0], vysledek[1])
	i+=1

np.savetxt("spoctene.txt", spoctene, fmt="%6g")