set encoding utf8
set terminal epslatex standalone
set output 'jen_data.tex' #výstupní soubor
#set ytics rotate by 90

set xlabel "t [s]"
set ylabel "spočtená hodnota $\kappa$"

set grid x
set grid y

set key top right

#set xrange [0:0.19]

plot './spoctene.txt' using 3:4:5 title "hodnota $\kappa$" with yerrorbars,
exit