import numpy as np
import math

data = np.loadtxt('data.txt')
n=data.shape[0]

suma = 0
for hodnota in data:
	suma += hodnota

prumer = suma/n

suma_odchylek_ctverec = 0
for hodnota in data:
	suma_odchylek_ctverec += (prumer - hodnota)**2
	print((prumer - hodnota)**2)
sigma = math.sqrt(suma_odchylek_ctverec/(n*(n-1)))
print(suma_odchylek_ctverec)

print(str(prumer) + ' +- ' + str(sigma))
