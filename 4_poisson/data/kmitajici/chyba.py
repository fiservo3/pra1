import math

V = 1.133e-3
sV = 1e-6

m = 4.59e-3
sm = 1e-5

r = 5.95e-3
sr = 5e-6

t = 0.3468
st = 0.0004

p = 98.16e3
sp = 0.01e3



a=sm*((V)/(t**2*p*r**4))
b=sV*((m)/(t**2*p*r**4))
c=2*st*((m*V)/(t**3*p*r**4))
d=sp*((m*V)/(t**2*p**2*r**4))
e=4*sr*((m*V)/(t**2*p*r**5))

print(a)
print(b)
print(c)
print(d)
print(e)

sigma = 4* math.sqrt(a**2+b**2+c**2+d**2+e**2)

print (sigma)