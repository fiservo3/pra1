#nastavení základních věcí pro gnuplot

set encoding utf8
set terminal eps
set output 'histogram.eps' #výstupní soubor

#nastavení věcí pro počítání binů
n=9 #počet 
max=876. #max value
min=858. #min value
width=(max-min)/n #interval width
#function used to map a value to the intervals
hist(x,width)=width*floor(x/width)+width/2.0



#nastavit, jak budou vypadat osy...

set xrange [min:max]
#set yrange [0:]

set offset graph 0.05,0.05,0.05,0.0#rámeček kolem grafu

set grid y

set xtics min,(max-min)/n,max
set boxwidth width*0.9
set style fill solid 0.5 #fillstyle
set tics out nomirror
set xlabel "počet pulsů za 300s [1]"
set ylabel "četnost"


#spočítat a vykreslit
plot "data.txt" u (hist($1,width)):(1.0) smooth freq w boxes lc rgb"blue" notitle
exit